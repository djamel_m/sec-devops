pipeline {
    agent any 
    tools {
        jdk 'jdk'
        nodejs 'nodejs'
    }
    environment  {
        SCANNER_HOME=tool 'sonar-scanner'
    }
    stages {
        stage('Cleaning Workspace') {
            steps {
                cleanWs()
            }
        }
        stage('Checkout from Git') {
            steps {
                git branch: 'master', url: 'https://gitlab.com/djamel_m/sec-devops.git'
            }
        }
        stage('Sonarqube Analysis') {
            steps {
                dir('Tetris-V1') {
                    withSonarQubeEnv('sonar-server') {
                        sh ''' $SCANNER_HOME/bin/sonar-scanner -Dsonar.projectName=TetrisVersion1.0 \
                        -Dsonar.projectKey=TetrisVersion1.0 '''
                    }
                }
            }
        }
        stage('Quality Check') {
            steps {
                script {
                    waitForQualityGate abortPipeline: false, credentialsId: 'sonarQ-token' 
                }
            }
        }
        stage('Installing Dependencies') {
            steps {
                dir('Tetris-V1') {
                    sh 'npm install'
                }
            }
        }
        stage('OWASP Dependency-Check Scan') {
            steps {
                dir('Tetris-V1') {
                    dependencyCheck additionalArguments: '--scan ./ --disableYarnAudit --disableNodeAudit', odcInstallation: 'DP-Check'
                    dependencyCheckPublisher pattern: '**/dependency-check-report.xml'
                }
            }
        }
        stage('Trivy File Scan') {
            steps {
                dir('Tetris-V1') {
                    sh 'trivy fs . > trivyfs.txt'
                }
            }
        }
        stage("Docker Image Build") {
            steps {
                script {
                    dir('Tetris-V1') {
                        withDockerRegistry(credentialsId: 'docker', toolName: 'docker') {   
                            sh 'docker system prune -f'
                            sh 'docker container prune -f'
                            sh 'docker build -t tetrisv1 .'
                        }
                    }
                }
            }
        }
        stage("Docker Image Pushing") {
            steps {
                script {
                    withDockerRegistry(credentialsId: 'docker', toolName: 'docker') {   
                        sh 'docker tag tetrisv1 djamelm/tetrisv1:${BUILD_NUMBER}'
                        sh 'docker push djamelm/tetrisv1:${BUILD_NUMBER}'
                    }
                }
            }
        }
        stage("TRIVY Image Scan") {
            steps {
                sh 'trivy image djamel_m/tetrisv1:${BUILD_NUMBER} > trivyimage.txt' 
            }
        }
        stage('Checkout Code') {
            steps {
                git branch: 'master', url: 'https://gitlab.com/djamel_m/sec-devops.git'
            }
        }
        stage('Update Deployment file') {
            environment {
                GIT_REPO_NAME = "sec-devops"
                GIT_USER_NAME = "djamel_m"
            }
            steps {
                dir('Manifest-file') {
                    withCredentials([string(credentialsId: 'gitlab', variable: 'GITHUB_TOKEN')]) {
                        sh '''
                            git config user.email "djamel@gmail.com"
                            git config user.name "djamel_m"
                            BUILD_NUMBER=${BUILD_NUMBER}
                            echo $BUILD_NUMBER
                            imageTag=$(grep -oP '(?<=tetrisv1:)[^ ]+' deployment-service.yml)
                            echo $imageTag
                            sed -i "s/tetrisv1:${imageTag}/tetrisv1:${BUILD_NUMBER}/" deployment-service.yml
                            git add deployment-service.yml
                            git commit -m "Update deployment Image to version \${BUILD_NUMBER}"
                            git push https://${GITHUB_TOKEN}@gitlab.com/${GIT_USER_NAME}/${GIT_REPO_NAME} HEAD:master
                        '''
                    }
                }
            }
        }
    }
}
